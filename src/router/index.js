import Vue from 'vue'
import Router from 'vue-router'
import Root from '@/components/Root'
import Input from '@/components/Input'
import Currency from '@/components/Currency'
import Results from '@/components/Results'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Домащняя',
      component: Root,
      meta: {
        title: 'Домащняя'
      }
    },
    {
      path: '/input',
      name: 'Ввод данных',
      component: Input,
      meta: {
        title: 'Ввод данных'
      }
    },
    {
      path: '/currency',
      name: 'Обмен',
      component: Currency,
      meta: {
        title: 'Обмен'
      }
    },
    {
      path: '/results',
      name: 'Результаты',
      component: Results,
      meta: {
        title: 'Результаты'
      }
    }
  ]
})
