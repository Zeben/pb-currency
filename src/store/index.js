/* eslint no-trailing-spaces: 0 */

import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    SYMBOLS: {
      'USD': '$',
      'RUR': '₽',
      'EUR': '€',
      'BTC': 'Ƀ'
    },
    currencies: [],
    histories: [],
    enteredMoney: ''
  },

  mutations: {
    pushData (state, { field, payload }) {
      state[field] = payload
    },

    pushToHistory (state, payload) {
      state.histories.push(payload)
    }
  },

  actions: {
    fetchCurrencies ({ commit }) {
      const url = 'https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11'
      fetch(url)
        .then(response => response.json())
        .then(response => {
          commit('pushData', { field: 'currencies', payload: response })
        })
    },

    performConversion ({ commit, state }, payload) {
      let found = this.getters.getCurrencyByCcy(payload)
      found.picked = state.enteredMoney
      found.result_ccy = (Number(state.enteredMoney) / found.buy).toFixed(2)
      found.symbol = state.SYMBOLS[found.ccy]
      commit('pushToHistory', found)
    }
  },

  getters: {
    getMoney: state => {
      return state.enteredMoney
    },

    getCurrencyByCcy: state => ccy => {
      return state.currencies.find(item => item.buy === ccy)
    },

    getLatestConversion: state => {
      return state.histories[state.histories.length - 1]
    }
  }
})
